info-beamer pi tools
====================

This is a collection of small but useful tools when
working with info-beamer pi:

screenshot - capture pi screenshot and save as JPEG

visual-watchdog - capture pi output and triggers if the output stays unchanged for too long

vncserver - a minimal vnc server for the raspberry

ibquery - query a running info-beamer

